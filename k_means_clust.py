import numpy as np

import pandas as pd


class KmeansCluster:
    def functioning():

        # Utility Function for finding the minimum distance

        def euclidian_dist(clust, datapoint):
            # x = np.repeat(datapoint, 3, axis=0)
            #print('datapoint', datapoint, 'clust', clust)
            return (np.argmin(np.sqrt((np.square(np.subtract(clust, datapoint))).sum(axis=1))))

        # def prepare_datasets():
        A, B, C = [(2, 10), (2, 5), (8, 4)], [(5, 8), (7, 5), (6, 4)], [(1, 2), (4, 9)]
        # Defining Cluster Head A,B,C
        history_centroids, datapoints = [], {0: [], 1: [], 2: []}

        for i, j in zip(A, B):
            datapoints[0].append(i)
            datapoints[1].append(j)
        for i in C:
            datapoints[2].append(i)

        for i in list(datapoints.keys()):
            datapoints[i].pop(datapoints[i].index(datapoints[i][0]))

        clust = A[0], B[0], C[0]

        datapoints_copy = {0: [clust[0]], 1: [clust[1]], 2: [clust[2]]}
        #print('Data Points before process', datapoints)
        for iteration in range(75):
            for i in datapoints:
                for j in range(len(datapoints[i])):
                    # Updating the datapoints of cluster
                    datapoints_copy[euclidian_dist(clust, datapoints[i][j])].append(datapoints[i][j])

            #print('Cluster Heads', (datapoints_copy))
            # print(datapoints)
            #   Updating the cluster head
            c = []
            for i in range(len(datapoints_copy)):
                datapoint_temp = (datapoints_copy[i])
                datapoint_median = (np.array([datapoint_temp]))
                arr = tuple(np.median(datapoint_temp, axis=0))
                c.append(arr)
            clust = tuple(c)
            for i in range(len(datapoints_copy)):
                datapoints_copy.update({i: [clust[i]]})

            #print('Cluster Heads', datapoints_copy)
            x_new = [*datapoints_copy[0], *datapoints_copy[1], *datapoints_copy[2]]

            #print('------------------------------------\n', 'Cluster Head after process', datapoints_copy, '\n------------------------------------------------\n')
            if iteration > 1:
                if x_new[0] == x_old[0] and x_new[1] == x_old[1] and x_new[2] == x_old[2]:
                    print('Stoped @ ', str(iteration) + 'th iteration')
                    result = x_new
                    break
            x_old = x_new
        # print(*x_new, datapoints)
        for i in range(len(datapoints)):
            datapoints[chr(i + 65)] = datapoints.pop(i)
        # print(np.array(datapoints))
        return datapoints


print('kmeans', KmeansCluster.functioning())

