import numpy as np
import pandas as pd

df = pd.read_csv('allelectronics.csv')
data = df.loc[:].values;print(data)
bc,bc_yes,bc_no = len(data[0])-1,0.0,0.0
for i in range(len(data)):
    if data[i][bc] == 'yes':bc_yes+=1
    elif data[i][bc] == 'no':bc_no+=1

yes_hyp = bc_yes/len(data)
no_hyp = bc_no/len(data)

print('Hypothesis for success or "Yes" is :',bc_yes/len(data))
print('Hypothesis for success or "No" is :',bc_no/len(data))
x1,x2,x3,x4 = [[0,0,0],[0,0,0]],[[0,0,0],[0,0,0]],[[0,0],[0,0]],[[0,0],[0,0]]
for i in range(len(data)):
    if data[i][1] == 'youth' and data[i][bc] == 'yes':x1[0][0]+=1
    elif data[i][1] == 'middle_aged' and data[i][bc] == 'yes':x1[0][1]+=1
    elif data[i][1] == 'senior' and data[i][bc] == 'yes':x1[0][2]+=1
    
    if data[i][1] == 'youth' and data[i][bc] == 'no':x1[1][0]+=1
    elif data[i][1] == 'middle_aged' and data[i][bc] == 'no':x1[1][1]+=1
    elif data[i][1] == 'senior' and data[i][bc] == 'no':x1[1][2]+=1
    
    if data[i][2] == 'high' and data[i][bc] == 'yes':x2[0][0]+=1
    elif data[i][2] == 'medium' and data[i][bc] == 'yes':x2[0][1]+=1
    elif data[i][2] == 'low' and data[i][bc] == 'yes':x2[0][2]+=1

    if data[i][2] == 'high' and data[i][bc] == 'no':x2[1][0]+=1
    elif data[i][2] == 'medium' and data[i][bc] == 'no':x2[1][1]+=1
    elif data[i][2] == 'low' and data[i][bc] == 'no':x2[1][2]+=1

    if data[i][3] == 'yes'  and data[i][bc] == 'yes':x3[0][0]+=1
    elif data[i][3] == 'no' and data[i][bc] == 'yes':x3[0][1]+=1

    if data[i][3] == 'yes'  and data[i][bc] == 'no':x3[1][0]+=1
    elif data[i][3] == 'no' and data[i][bc] == 'no':x3[1][1]+=1

    if data[i][4] == 'fair'  and data[i][bc] == 'yes':x4[0][0]+=1
    elif data[i][4] == 'excellent' and data[i][bc] == 'yes':x4[0][1]+=1

    if data[i][4] == 'fair'  and data[i][bc] == 'no':x4[1][0]+=1
    elif data[i][4] == 'excellent' and data[i][bc] == 'no':x4[1][1]+=1 
print(x1,x2,x3,x4)
print('Data You Want to Predict: \n')  
age = input("Enter The Age ['youth','middle_aged','senior']: ")
income = input("Enter the Income['high','medium','low']: ")
student = input("Whether He is a  Student ['yes','no']: ")
credit = input("Enter the Credit Rating ['Fair','excellent']: ")

result_yes,result_no= 1,1
if age == 'youth':result_yes*=x1[0][0];result_no*=x1[1][0]
elif age == 'middle_aged':result_yes*=x1[0][1];result_no*=x1[1][1]
elif age == 'senior':result_yes*=x1[0][2];result_no*=x1[1][2]

if income == 'high':result_yes*=x2[0][0];result_no*=x2[1][0]
elif income == 'medium':result_yes*=x2[0][1];result_no*=x2[1][1]
elif income == 'low':result_yes*=x2[0][2];result_no*=x2[1][2]

if student == 'yes':result_yes*=x3[0][0];result_no *= x3[1][0]
elif student == 'no':result_yes*=x3[0][1];result_no *= x3[1][1]

if credit == 'fair':result_yes*=x4[0][0];result_no *= x4[1][0]
elif credit == 'excellent':result_yes*=x4[0][1];result_no *= x4[1][1]

yes_probability,no_probability = (result_yes/bc_yes**4)*yes_hyp,(result_no/bc_no**4)*no_hyp
print(yes_probability,no_probability)
print('The Given Sample will Buy Computer') if yes_probability > no_probability else print('The Given Sample will not Buy Computer')
