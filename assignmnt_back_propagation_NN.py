import numpy as np
l,i = 0.9,0
def sigmoid_activation(step):
	return 1/(1+np.exp(-step))

weight_ip = 2*np.random.random((2,3))-1
weight_hid = 2*np.random.random(2,)-1
inp = np.array([[1],
				[0],
				[1]])
expected_op = np.array([1])
bias_ip = 2*np.random.random((2,1))-1
bias_hid = 2*np.random.random((1,1))-1
error=[]
while 1:
	i+=1
	layer1 = sigmoid_activation(np.dot(weight_ip,inp) + bias_ip)#Contains {O-4,O-5}
	layer2 = sigmoid_activation(np.dot(weight_hid,layer1)+ bias_hid)#Contains {O-6}

	error_layer2 = layer2*(1 - layer2)*(expected_op - layer2)
	error_layer1 = error_layer2*(np.multiply(np.multiply(layer1,1-layer1),
								weight_hid.reshape(2,1)))
	error_delta_layer2 = l*(error_layer2)*layer1
	weight_hid_trnspose = weight_hid.reshape(2,1)
	weight_hid_trnspose += error_delta_layer2;weight_hid_trnspose.reshape(1,2);
	weight_hid = weight_hid_trnspose.reshape(1,2)
	
	error_delta_layer2 = l*(np.dot(error_layer1,inp.reshape(1,3)))
	weight_ip += error_delta_layer2
	
	delta_bias_op = l*error_layer2
	bias_hid += delta_bias_op
	
	delta_bias_ip = l*error_layer1
	bias_ip += delta_bias_ip
	if i%100000 == 0:
		print('__________________________________\n')
		error_layer2 = ' '.join(map(str,error_layer2.reshape(1,)))
		error_layer1 = ' '.join(map(str,error_layer1.reshape(2,)))
		print('error_layer2:',error_layer2)
		print('error_layer1:',error_layer1)
		print()
		error.append(error_layer2)
	if i == 600000:break
print(error)