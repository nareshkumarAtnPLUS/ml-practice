import numpy as np
import matplotlib.pyplot as plt

# Utility Function for finding the minimum distance


def euclidian_dist(clust, datapoint):
    # x = np.repeat(datapoint, 3, axis=0)
    return (np.argmin(np.sqrt((np.square(np.subtract(clust, datapoint))).sum(axis=1))))


# def prepare_datasets():
A, B, C = [(2, 10), (2, 5), (8, 4)], [(5, 8), (7, 5), (6, 4)], [(1, 2), (4, 9)]
# Defining Cluster Head A,B,C
history_centroids, datapoints = [], {0: [], 1: [], 2: []}

for i, j in zip(A, B):
    datapoints[0].append(i)
    datapoints[1].append(j)
for i in C:
    datapoints[2].append(i)

for i in list(datapoints.keys()):
    datapoints[i].pop(datapoints[i].index(datapoints[i][0]))

clust = A[0], B[0], C[0]


clust_head = {'A': clust[0], 'B': clust[1], 'C': clust[2]}
datapoints_copy = {0: [(2, 10)], 1: [(5, 8)], 2: [(1, 2)]}
print('Cluster_head before process', clust)
for i in datapoints:
    for j in range(len(datapoints[i])):
        print(datapoints[i][j])
        print(euclidian_dist(clust, datapoints[i][j]))
        datapoints_copy[euclidian_dist(clust, datapoints[i][j])].append(datapoints[i][j])

print((datapoints_copy))
print(datapoints)
#	Updating the cluster head
c = []
for i in range(len(datapoints_copy)):
    datapoint_temp = (datapoints_copy[i])
    datapoint_median = (np.array([datapoint_temp]))
    arr = tuple(np.median(datapoint_temp, axis=0))
    c.append(arr)
clust = tuple(c)
print('Cluster Head after process', clust)

xA, yA = zip(*A)
xB, yB = zip(*B)
xC, yC = zip(*C)
