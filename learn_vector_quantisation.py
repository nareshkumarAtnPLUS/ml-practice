import numpy as np

class LinearVectorQuantization:
    def functioning():
        Weights = np.array([[1, 1, 0, 0],
                            [0, 0, 0, 1]], dtype=float)

        X = np.array([[0, 0, 1, 1],
                      [1, 0, 0, 0],
                      [0, 1, 1, 0]])
        label = [1, 2, 2, 1, 2]
        classLabel = np.array(label, dtype=int)
        alpha = 0.1
        # utility function

        def euclidian_dist(clust, datapoint):
            # x = np.repeat(datapoint, 3, axis=0)
            # print('datapoint', datapoint, 'clust', clust,'Distance',(np.sqrt((np.square(np.subtract(clust, datapoint))).sum(axis=1))))
            return (np.argmin(np.sqrt((np.square(np.subtract(clust, datapoint))).sum(axis=1))))

        def weightIncreament(Weight, alpha, ip_vector):
            Weight += alpha * np.subtract(ip_vector, Weight)
            return Weight

        def weightDecreament(Weight, alpha, ip_vector):
            Weight -= alpha * np.subtract(ip_vector, Weight)
            return Weight

        flag = 0
        weight_new = [[], []]
        weight_old = [[], []]
        for iterating in range(100):
            for itr_ip_vector in range(len(X)):

                ip_vector = X[itr_ip_vector]
                #print(itr_ip_vector)
                J = euclidian_dist(ip_vector, Weights)
                #print(classLabel[itr_ip_vector + 2], J + 1, Weights[J], ip_vector, alpha * np.subtract(ip_vector, Weights[J]), classLabel[itr_ip_vector])
                if classLabel[itr_ip_vector + 2] == J + 1:
                    Weights[J] = weightIncreament(Weights[J], alpha, ip_vector)
                else:
                    Weights[J] = weightDecreament(Weights[J], alpha, ip_vector)
                alpha /= 2
                #print((Weights[J]))
                weight_new[J] = ((Weights[J]).tolist())
                if iterating > 1:
                    flag = 0
                    for k in range(len(weight_new[J])):
                        if weight_new[0][k] == weight_old[0][k] and weight_new[1][k] == weight_old[1][k]:
                            #print("\n\n", iterating, weight_new[1][k], weight_old[1][k], sep='\n--------\n')
                            flag += 1
                            #print(flag)
                    if flag == 4:
                        print('Stoped @ ', str(iterating) + 'th iteration\n___________________________________________________________')
                        return Weights, weight_new, weight_old,X

                weight_old[J] = weight_new[J]


if __name__ == '__main__':
    print(*LinearVectorQuantization.functioning(),sep="\n___________________________________________________________\n")
