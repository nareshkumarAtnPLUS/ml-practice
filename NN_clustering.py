import numpy as np
class NeuralNetworkClustering:
    def functioning(v1,v2,v3,v4,alpha):
        vector = np.array([v1,v2,v3,v4],dtype=float)
        w = np.array([[0.2,0.8],
                    [0.6,0.4],
                    [0.5,0.7],
                    [0.9,0.3]],dtype=float)
        cluster = []


        def equalidian(vector,weights):
            w1,w2 = weights.T
            w1,w2 = w1.reshape(1,4),w2.reshape(1,4)
            return ((np.sqrt(np.sum(np.square(vector - w1)))),
                        (np.sqrt(np.sum(np.square(vector - w2)))))
        for iteration in range(100):
            for i in range(4):
                d1,d2 = (equalidian(vector[i],w))
                J = np.argmin(np.array([d1,d2]))
                w1,w2 = w.T
                w1,w2 = w1.reshape(1,4),w2.reshape(1,4)
                if J==0:
                    w1 = w1 + alpha*(vector[i] - w1)
                if J==1:
                    w2 = w2 + alpha*(vector[i] - w2)
                x = [(w1.ravel()).tolist(),(w2.ravel()).tolist()]
                w = (np.array(x).T)
                #print('w',w)
                if iteration == 99:
                    d1,d2 = (equalidian(vector[i],w))
                    J = np.argmin(np.array([d1,d2]))
                    #print('J at 99 = ',J+1)
                    cluster.append(J+1)
                    #print('Weights',w,sep="\n")
            alpha/=2
        print('___________________________\n','Cluster of the Given Vectors are:\n',*cluster)
        return vector,cluster
v1 = [int(x) for x in input("Enter the input Vector v1 : ").split()]
v2 = [int(x) for x in input("Enter the input Vector v2 : ").split()]
v3 = [int(x) for x in input("Enter the input Vector v3 : ").split()]
v4 = [int(x) for x in input("Enter the input Vector v4 : ").split()]
alpha = float(input('Enter The Learning Rate : '))
print('ip',)
print(NeuralNetworkClustering.functioning(v1,v2,v3,v4,alpha))
